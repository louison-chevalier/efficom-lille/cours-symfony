<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210423100423 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment ADD convention_id INT NOT NULL, ADD photo_file_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CA2ACEBCC FOREIGN KEY (convention_id) REFERENCES convention (id)');
        $this->addSql('CREATE INDEX IDX_9474526CA2ACEBCC ON comment (convention_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CA2ACEBCC');
        $this->addSql('DROP INDEX IDX_9474526CA2ACEBCC ON comment');
        $this->addSql('ALTER TABLE comment DROP convention_id, DROP photo_file_name');
    }
}

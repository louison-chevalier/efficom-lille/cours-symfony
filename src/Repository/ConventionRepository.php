<?php

namespace App\Repository;

use App\Entity\Convention;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Convention|null find($id, $lockMode = null, $lockVersion = null)
 * @method Convention|null findOneBy(array $criteria, array $orderBy = null)
 * @method Convention[]    findAll()
 * @method Convention[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConventionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Convention::class);
    }

    /**
     * @return Convention[] Returns an array of Convention objects
     */
    public function findByInternational($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.international = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findWithCommentsWithPhotos($international, $minCommWithPhoto)
    {
        return $this->createQueryBuilder('cv')
            ->andWhere('cv.international = :val')
            ->andWhere('cm.photoFileName IS NOT NULL')
            ->join('cv.comments', 'cm')
            ->having('COUNT(cm.id) >= :minCommWithPhoto')
            ->groupBy('cv.id')
            ->setParameter('val', $international)
            ->setParameter('minCommWithPhoto', $minCommWithPhoto)
            ->getQuery()
            ->getResult()
        ;
    }
}

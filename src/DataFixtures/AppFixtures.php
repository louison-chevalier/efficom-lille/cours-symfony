<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Convention;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    const CITIES = [
        'London',
        'Paris',
        'Bruxelles',
        'Madrid',
        'Berlin'
    ];

    const YEARS = [2018, 2019, 2020, 2021];

    const TEXTS = [
        'C\'était pas mal !!!',
        'Great convention!!!',
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
    ];

    const PHOTOS = [
        'conf-1.jpg',
        'conf-2.jpg',
        'conf-3.jpg',
        'conf-4.jpg',
    ];

    const PSEUDOS = [
        'foo',
        'bar',
        'baz',
        'qux',
    ];

    const DOMAINS = [
        'gmail.com',
        'orange.fr',
        'free.fr',
        'yahoo.com',
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::CITIES as $city) {
            foreach (self::YEARS as $year) {
                $convention = new Convention();
                $convention->setCity($city)
                    ->setYear($year)
                    ->setInternational((bool)rand(1, 2));

                $nbComments = $city === 'Paris' && $year === 2021 ? 0 : rand(5, 30);
                for ($i = 0; $i < $nbComments; $i++) {
                    $comment = new Comment();
                    $comment->setAuthor(self::PSEUDOS[array_rand(self::PSEUDOS)])
                        ->setCreatedAt(new \DateTime('-' . rand(1, 1000) . ' hours'))
                        ->setEmail(self::PSEUDOS[array_rand(self::PSEUDOS)] . '@' . self::DOMAINS[array_rand(self::DOMAINS)])
                        ->setText(self::TEXTS[array_rand(self::TEXTS)])
                        ->setPhotoFileName(self::PHOTOS[array_rand(self::PHOTOS)]);

                    $convention->addComment($comment);
                }

                $manager->persist($convention);
            }
        }

        $manager->flush();
    }
}

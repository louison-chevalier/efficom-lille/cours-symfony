<?php

namespace App\Controller;

use App\Entity\Convention;
use App\Repository\CommentRepository;
use App\Repository\ConventionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConventionController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(ConventionRepository $repository): Response
    {
        return $this->render('convention/list.html.twig', [
            'objects' => $repository->findAll(),
        ]);
    }

    #[Route('/convention/{id}', name: 'convention')]
    public function show(Request $request, Convention $convention, CommentRepository $repository)
    {
        $offset = max(0, $request->query->getInt('offset', 0));
        $paginator = $repository->getCommentPaginator($convention, $offset);
        return $this->render('convention/show.html.twig', [
            'convention' => $convention,
            'comments' => $paginator,
            'previous' => $offset - CommentRepository::PAGINATOR_PER_PAGE,
            'next' => min(count($paginator), $offset + CommentRepository::PAGINATOR_PER_PAGE),
        ]);
    }
}
